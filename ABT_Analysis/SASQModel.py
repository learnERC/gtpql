import os
import pygraphviz as pgv
from ABT_Action import ABT_Action
from ABT_State import ABT_State

class SASQModel:
    def __init__(self, q_model_JSON):
        self.serialized_locations_JSON = q_model_JSON["SerializedLocations"]["$values"]
        self.model = list()
        self.actions_number = 0
        self.SAS_tuples = 0
        self.total_q_value = 0.0
        self.average_q_value = 0.0
        self._populate_model()
        self.graph = None 

    def write_graph(self, outputpath, outputname = "QModel", plot_everything= False):
        self._populate_graph(plot_everything)
        # self.graph.layout("dot")
        self.graph.layout("neato")
        self.graph.write(os.path.join(outputpath, outputname + ".dot"))
        self.graph.draw(os.path.join(outputpath, outputname + ".pdf"))


    def _populate_graph(self, plot_everything):
        self.graph = pgv.AGraph(strict = False, directed=True)#, concentrate= True)
        to_write = dict()
        for SAS_tuple in self.model:
            state = SAS_tuple[0]
            action = SAS_tuple[1]
            dest_state = SAS_tuple[2]
            q_value = SAS_tuple[3]
            visits_count = SAS_tuple[4]
            if state in to_write:
                if plot_everything:
                    to_write[state].append(SAS_tuple)
                else:
                    if q_value == to_write[state][0][3]:
                        to_write[state].append(SAS_tuple)
                    elif q_value > to_write[state][0][3]:
                        to_write[state] = [SAS_tuple]
            elif q_value > 0 or plot_everything:
                to_write[state] = [SAS_tuple]
            # else:
            #     to_write[state] = [SAS_tuple]
            
            # if ("e-mail" in action.action_name.lower() or "email" in action.action_name.lower()) and q_value > 0:
            #     if state in to_write:
            #         to_write[state].append(SAS_tuple)
            #     else:
            #         to_write[state] = [SAS_tuple]
        unknown = 1
        for SAS_tuple_list in to_write.values():
            for SAS_tuple in SAS_tuple_list:
                state = SAS_tuple[0]
                action = SAS_tuple[1]

                state_color = "black"
                if state.gave_reward is True:
                    state_color = "red"

                dest_state = SAS_tuple[2]
                q_value = SAS_tuple[3]
                visits_count = SAS_tuple[4]

                dest_state_color = "black"
                if dest_state.gave_reward is True:
                    dest_state_color = "red"

                action_type = "C"
                if action.fill_actions is None:
                    action_type = "S"

                self.graph.add_node(state.hashcode, label = state.hashcode, color=state_color)
                if dest_state.hashcode == '':
                    dest_state.hashcode = "UNKNOWN STATE #" + str(unknown)
                    unknown +=1
                self.graph.add_node(dest_state.hashcode, label = dest_state.hashcode, color=dest_state_color)
                self.graph.add_edge(state.hashcode, dest_state.hashcode, weight= q_value, label= "{}-{}: {},{}".format(visits_count,action_type, action.action_name,q_value))
            

    def _populate_model(self):
        for serialized_location in self.serialized_locations_JSON:
            state = ABT_State(serialized_location["Key"])
            for action_dest_state_value_JSON in serialized_location["Value"]["$values"]:
                action = ABT_Action(action_dest_state_value_JSON["Key"])
                for dest_state_value_JSON in action_dest_state_value_JSON["Value"]["$values"]:
                    dest_state = ABT_State(dest_state_value_JSON["Key"])
                    q_value = round(float(dest_state_value_JSON["Value"]["Item1"]), 6)
                    visits_count = int(dest_state_value_JSON["Value"]["Item2"])
                    self.model.append((state, action, dest_state, q_value, visits_count))
                    self.actions_number += 1
                    self.total_q_value += q_value
                    self.SAS_tuples += 1
        if self.SAS_tuples != 0:
            self.average_q_value = self.total_q_value / self.SAS_tuples