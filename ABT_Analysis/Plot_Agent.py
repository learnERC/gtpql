import sys
from QAgent import QAgent

def main(argv):
    q_agent_path = sys.argv[1]
    q_agent = QAgent(q_agent_path)
    q_agent.q_model.write_graph("", plot_everything=False)
    total_states_number = len(q_agent.q_model.serialized_locations_JSON)
    total_sas_number = q_agent.q_model.SAS_tuples

    print("#States: " + str(total_states_number))
    print("#SAS Tuples: " + str(total_sas_number))
    found = dict()
    for location in q_agent.q_model.serialized_locations_JSON:
        if location["Key"]["StateHashCode"] in found.keys():
            print("DOUBLE")
        else: 
            found[location["Key"]["StateHashCode"]] = location
    print ("Done")


if __name__ == "__main__":
    main(sys.argv)