import os
import json
from numpy import empty
import pygraphviz as pgv
from QModel import QModel
from SASQModel import SASQModel
from Trajectory import Trajectory

class QAgent:
    def __init__(self, q_agent_file_path):
        self._q_agent_file_path = q_agent_file_path
    
        self._load_file()
        self._load_trajectory()
        self._load_q_model()

    def _load_file(self):
        with open(self._q_agent_file_path, 'r') as f:
            json_string = json.load(f)
        self._JSON_q_agent = json_string
        #return json_string
    
    def _load_trajectory(self):
        self._JSON_trajectory = self._JSON_q_agent["trajectory"]
        self.trajectory = Trajectory(self._JSON_trajectory)
    
    def _load_q_model(self):
        self._JSON_q_model = self._JSON_q_agent["qFunction"]

        if "SASQFunction" in self._JSON_q_model["$type"]:
            self.q_model = SASQModel(self._JSON_q_model)
        else:
            self.q_model = QModel(self._JSON_q_model)
  