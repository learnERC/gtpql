import shutil
import os
import sys
import pickle
from xmlrpc.server import list_public_methods
import matplotlib.pyplot as plt
from QAgent import QAgent
import csv
import pandas as pd
import numpy as np
from natsort import natsorted



class ABT_Analyser:

    def __init__(self, experiment_folder):
        self.experiment_folder = experiment_folder
        self.metrics_folder = self.experiment_folder + os.path.sep + "metrics"
        self.target_runs_folder = experiment_folder + os.path.sep + "targets_runs"

    def run(self):
        self._analysis_setup()
        self._run_okko_analysis()
        self._run_aggregated_qmodel_analysis()

    def _analysis_setup(self):
        shutil.rmtree(self.metrics_folder, ignore_errors=True)
        os.makedirs(self.metrics_folder)
        epochs_dirs = [f.path for f in os.scandir(self.target_runs_folder) if f.is_dir()]
        if len(epochs_dirs) <= 1:
            zip_files = [f.path for f in os.scandir(self.target_runs_folder) if ".zip" in f.name]
            for zip_file in zip_files:
                os.system("cd " + self.target_runs_folder + "; unzip " + zip_file)
            os.system("chmod u+x -R " + self.target_runs_folder)

    def _run_aggregated_qmodel_analysis(self):
        list_epochs_with_paths = [f.path for f in os.scandir((self.experiment_folder)) if f.is_dir() and f.name != "metrics" and f.name != "targets_runs"]
        list_epochs_with_paths = natsorted(list_epochs_with_paths)
        last_q_agent = None
        total_qvalue_list = list()
        avg_value_list = list()
        for epoch_path in list_epochs_with_paths:
        # for i in range(len(list_epochs_with_paths)-2 ): # -1 metrics folder -1 target_runs folder
            # epoch_path = os.path.join(self.experiment_folder, str(i))
            if (os.path.basename(epoch_path) != os.path.basename(self.metrics_folder)):
                last_q_agent = self._run_epoch(epoch_path, last_q_agent)
                total_qvalue_list.append(last_q_agent.q_model.total_q_value)
                avg_value_list.append(last_q_agent.q_model.average_q_value)
        self._plot_list(total_qvalue_list, os.path.join(self.metrics_folder, "Total_QValue"))
        self._plot_list(avg_value_list, os.path.join(self.metrics_folder, "Average_QValue"))

    def _create_q_model_graph(self):
        q_agent_path = self.experiment_folder + "/QAgent.json"
        q_agent = QAgent(q_agent_path)
        print(len(q_agent.trajectory.elements))

    def _plot_list(self, list_to_plot, name):
        plt.clf()
        base_name = os.path.basename(name)
        with open(name, "wb") as outfile:
            pickle.dump(list_to_plot, outfile)
        plt.plot(list_to_plot, label=base_name)
        plt.legend()
        plt.ticklabel_format(style="plain")
        plt.xlabel("Epochs")
        plt.ylabel("Q Value")
        plt.savefig(name+".pdf", bbox_inches='tight', transparent="True", pad_inches=0.1)
        plt.close()

    def _run_epoch(self, epoch_path, last_q_agent):
        epoch_number =os.path.basename(epoch_path)
        print("EPOCH " + epoch_number + ":")

        cur_epoch_metrics_path = self.metrics_folder + os.path.sep + str(epoch_number)
        os.makedirs(cur_epoch_metrics_path)

        list_subfolders_with_paths = [f.path for f in os.scandir(epoch_path) if f.is_dir()]
        sub = "RUN"
        run_folder = next((s for s in list_subfolders_with_paths if sub in s), None)
        q_agent_path = run_folder + "/QValues/QAgent.json"
        q_agent = QAgent(q_agent_path)
        q_agent.q_model.write_graph(cur_epoch_metrics_path)

        hosts_qagents_folder_path = epoch_path + os.path.sep + "QAgents"
        for f in os.scandir(hosts_qagents_folder_path):
            simple_name = f.name.split('.')[0]
            host_agent = QAgent(f.path)
            host_agent.trajectory.write_graph(cur_epoch_metrics_path, simple_name, True)
        return q_agent

    def _run_okko_analysis(self):
        epochs_dirs = [f.path for f in os.scandir(self.target_runs_folder) if f.is_dir() and f.name.isnumeric()]
        epochs_dirs = natsorted(epochs_dirs)
        results_file = self.metrics_folder + os.path.sep + "okko_results.csv"
        with open(results_file, 'w') as f:
            writer = csv.writer(f)
            first = True
            # for epoch in range(len(epochs_dirs)):
            for epoch_dir in epochs_dirs:
                # epoch_dir = os.path.join(self.target_runs_folder, str(epoch))
                hosts_dirs  = [f.path for f in os.scandir(epoch_dir) if f.is_dir()]
                
                metrics = dict()
                for host_dir in hosts_dirs:
                    try:
                        run_dir = [f.path for f in os.scandir(host_dir) if f.is_dir() and "RUN" in f.name]
                        episodes_dir = run_dir[0] + os.path.sep + "episodes"
                        episodes = [f.path for f in os.scandir(episodes_dir) if f.is_dir()]
                        sorted_episodes = sorted(episodes, reverse = True)

                        metrics_file = [f.path for f in os.scandir(sorted_episodes[0]) if f.is_file and f.name == "metrics.txt"][0]
                        host_metrics = self.__get_okko_metrics(metrics_file)
                        metrics = self.__merge_metrics_dicts(metrics, host_metrics)   
                    except:
                        print("Invalid metrics file, maybe a failed episode... ")
                        print(run_dir)
                
                if first:
                    header = list()
                    header.append("Epoch")
                    for key in metrics.keys():
                        header.append("{}_OK".format(key))
                        header.append("{}_KO".format(key))
                    writer.writerow(header)
                    first = False

                row = list()
                row.append(os.path.basename(epoch_dir))
                for key in metrics.keys():
                    row.extend(list(metrics.get(key)))
                writer.writerow(row)
        
        data = pd.read_csv(results_file)
        data = data.loc[:, (data != 0).any(axis=0)] ## REMOVES COLUMNS WITH ONLY 0s

        data = data.sort_values(["Epoch"], ascending = True)
    
        totals = data[["Epoch", "total_OK", "total_KO"]].copy()
        totals = totals.sort_values(["Epoch"], ascending = True)
        
        z = np.polyfit(totals.index.values, list(totals['total_OK']), 1)
        #z = np.polyfit(totals['Epoch'], list(totals['total_OK']), 1)

        p = np.poly1d(z)
        # y = np.polyfit(totals.index.values, list(totals['total_KO']), 1)
        y = np.polyfit(totals['Epoch'], list(totals['total_KO']), 1)
        q = np.poly1d(y)

        del data["total_OK"]
        del data["total_KO"]
        ax = data.plot.line(x='Epoch')
        # ax.set_xticks(range(len(epochs_dirs)))
        plt.plot()

        plt.legend(bbox_to_anchor=(1,1), loc="upper left")
        plt.savefig(self.metrics_folder + os.path.sep + 'OK-KO.pdf', bbox_inches='tight')
        print(data)

        ax = totals.plot.line(x='Epoch')
        # ax.set_xticks(range(len(epochs_dirs)))
        plt.plot()        
        
        ax.plot(totals['Epoch'],p(totals['Epoch']),"r--")
        ax.plot(totals['Epoch'],q(totals['Epoch']),"r--")

        plt.legend(bbox_to_anchor=(1,1), loc="upper left")
        plt.savefig(self.metrics_folder + os.path.sep + 'Total_OK-KO.pdf', bbox_inches='tight')
        print(data)
        plt.close()


    def __merge_metrics_dicts(self, metrics, host_metrics):
        for key in host_metrics.keys():
            metrics.get(key, (0, 0))
            metrics[key] = tuple(sum(t) for t in zip(metrics.get(key, (0, 0)), host_metrics.get(key)))
        return metrics


    def __get_ok_ko(self, line):
        line = line.lstrip()
        name_start_index = line.find(" ") + 1
        name_end_index = line[name_start_index:].find(" -") + name_start_index

        if line[:name_end_index].find(" ") != -1:
            name = line[name_start_index:name_end_index]
        else:
            name = line[:name_end_index]
        ok_index = line.find("#OK: ")
        ko_index = line.find("#KO: ")
        ok = int(line[ok_index+5: ko_index].strip())
        ko = int(line[ko_index+5].strip())
        return name, ok, ko

    def __get_okko_metrics(self, metrics_file):
        with open(metrics_file) as f:
            lines = [line.rstrip() for line in f]
            metrics_start = 0
            for line in lines:
                metrics_start += 1
                if "# of OK/KO Functionalities:" in line:
                    break
            metrics = dict()
            metrics["total"] = (0,0)
            for i in range(metrics_start, len(lines)):
                metric_name, metric_ok, metric_ko = self.__get_ok_ko(lines[i])
                metrics[metric_name] = (metric_ok, metric_ko)
                metrics["total"] = (metrics["total"][0]+metric_ok, metrics["total"][1]+metric_ko)
            return metrics

def main(argv):
    experiment_folder = argv[1]
    analyser = ABT_Analyser(experiment_folder)
    analyser.run()


if __name__ == "__main__":
    main(sys.argv)