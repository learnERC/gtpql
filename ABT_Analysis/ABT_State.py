class ABT_State:
    def __init__(self, state_json):
        self.state_json = state_json
        self.id = state_json["id"]
        self.gave_reward = state_json["GaveReward"]
        self.hashcode = state_json["StateHashCode"]
        try:
            self.url = state_json["extraProperties"]["url"]
        except:
            self.url = ""
            pass

    def _get_state_json(self):
        return self._state_json
    
    def _set_state_json(self, value):
        self._state_json = value

    state_json = property(_get_state_json, _set_state_json)

    def __eq__(self, other):
        return self.id == other.id

    # def __eq__(self, other):
    #     return (self.id, frozenset(self.state_json)) == (other.id, frozenset(self.state_json))

    def __ne__(self, other):
        x = self.__eq__(other)
        if x is not NotImplemented:
            return not x
        return NotImplemented

    def __hash__(self):
        return hash((self.id, frozenset(self.state_json)))

    def __str__(self):
        if (self.hashcode):
            return self.hashcode
        if (self.url):
            return self.url
        return "{S: " + str(self.id) + "}"