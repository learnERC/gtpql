from ABT_Action import ABT_Action
from ABT_State import ABT_State

class QModel:
    def __init__(self, q_model_JSON):
        self.serialized_locations_JSON = q_model_JSON["SerializedLocations"]["$values"]
        self.model = list()
        self.states_number = 0
        self.actions_number = 0
        self.state_action_pairs = 0
        self.total_q_value = 0.0
        self.average_q_value = 0.0
        self._populate_model()
        self.graph = None 
        self._populate_graph()

    def write_graph(self, outputpath):
        pass

    def _populate_model(self):
        for serialized_location in self.serialized_locations_JSON:
            state = ABT_State(serialized_location["Key"])
            self.states_number += 1
            for action_value_JSON in serialized_location["Value"]["$values"]:
                action = ABT_Action(action_value_JSON["Key"])
                q_value = float(action_value_JSON["Value"])
                self.model.append((state, action, q_value))
                self.actions_number += 1
                self.total_q_value += q_value
                self.state_action_pairs += 1
        if self.state_action_pairs != 0:
            self.average_q_value = self.total_q_value / self.state_action_pairs
    
    def _populate_graph(self):
        pass