class ABT_Action:
    def __init__(self, action_JSON):
        self.click_action, self.fill_actions = self._get_actions(action_JSON)
        
        if "Name" in self.click_action["target"]["webElementState"].keys() and self.click_action["target"]["webElementState"]["Name"].strip() != "":
            self.action_name = self.click_action["target"]["webElementState"]["Name"]

        elif "Label" in self.click_action["target"]["webElementState"].keys() and self.click_action["target"]["webElementState"]["Label"].strip() != "":
            self.action_name = self.click_action["target"]["webElementState"]["Label"]
        
        elif "Id" in self.click_action["target"]["webElementState"].keys() and self.click_action["target"]["webElementState"]["Id"].strip() != "":
            self.action_name = self.click_action["target"]["webElementState"]["Id"]
        else:
            self.action_name = ""
    
    def _get_actions(self, action_JSON):
        click_action = None
        fill_actions = None
        if 'Name' in action_JSON.keys():
            click_action = action_JSON
        
        elif '$values' in action_JSON.keys():
            fill_actions = action_JSON['$values']
            click_action = fill_actions[len(fill_actions)-1]
            del fill_actions[len(fill_actions)-1]
        return [click_action, fill_actions]

    def __eq__(self, other):
        if isinstance(other, ABT_Action):
            if self.action_name != other.action_name:
                return False
            if self.click_action != other.click_action:
                return False
            if self.fill_actions != other.fill_actions:
                return False
            return True
        return NotImplemented

    def __ne__(self, other):
        x = self.__eq__(other)
        if x is not NotImplemented:
            return not x
        return NotImplemented

    def __hash__(self):
        if self.fill_actions:
            return hash((self.action_name,frozenset(self.click_action), frozenset(self.fill_actions)))
        return hash((self.action_name,frozenset(self.click_action)))

    
    def __str__(self):
        return "{ClickAction: " + str(self.click_action) + "FillActions: " + str(self.fill_actions) + "}"

    def __remove_parameters_from_string(self, s):
        start = s.find( '(' )
        end = s.find( ')' )
        if start != -1 and end != -1:
            return  s[0: start]
        return s

    def _remove_element_from_action(element, action):
        if element in action:
            action[element] = ''
    
    def _remove_elements_from_action(elements, action):
        for element in elements:
            ABT_Action._remove_element_from_action(element,action)

    def __prune_target(action):
        wes = action['target']['webElementState']
        if 'SelectedIndex' in wes:
            wes['SelectedIndex'] = ""
        if 'Selected' in wes:
            wes['Selected'] = ""
        if 'Text' in wes and wes['Text'] != "":
            wes['Text'] = ""

    def _prune_actions(self):
        self.action_name = self.__remove_parameters_from_string(self.action_name)
        self.click_action['Name'] = self.__remove_parameters_from_string(self.click_action['Name'])

        elements_to_remove = ["Parameter", "values"]
        ABT_Action._remove_elements_from_action(elements_to_remove, self.click_action)
        ABT_Action.__prune_target(self.click_action)
        
        if self.fill_actions is not None:
            for action in self.fill_actions:
                action['Name'] = self.__remove_parameters_from_string(action['Name'])
                ABT_Action._remove_elements_from_action(elements_to_remove, action)
                ABT_Action.__prune_target(action)
        
        
    