import os
from SARSA import SARSA
import pygraphviz as pgv


class Trajectory:
    def __init__(self, trajecotry_JSON):
        self._trajecotry_JSON = trajecotry_JSON
        self.elements = self._init_elements()
        self.graph = Trajectory_Graph(self.elements)
        self.episodes_graphs = self._init_episodes_graphs()

    def _init_elements(self):
        elements = list()
        if '$values' in self._trajecotry_JSON.keys():
            for sarsa_JSON in self._trajecotry_JSON["$values"]:
                elements.append(SARSA(sarsa_JSON))
        elif 'List' in self._trajecotry_JSON.keys():
            for sarsa_JSON in self._trajecotry_JSON["List"]["$values"]:
                elements.append(SARSA(sarsa_JSON))
        return elements

    @property
    def length(self):
        return len(self.elements)

    def _init_episodes_graphs(self):
        episodes_graphs = list()
        
        episode_sarsa_list = list()
        for sarsa_element in self.elements:
            reward = sarsa_element.reward
            episode_sarsa_list.append(sarsa_element)
            if reward > 0:
                episodes_graphs.append(Trajectory_Graph(episode_sarsa_list))
                episode_sarsa_list = list()

        return episodes_graphs

    def write_graph(self, outputpath, outputname, episodes_graphs = False):
        self.graph.write(os.path.join(outputpath, outputname))
        if episodes_graphs:
            episodes_graphs_folder = os.path.join(outputpath, outputname)
            os.makedirs(episodes_graphs_folder)
            episode_number = 0
            for episode_graph in self.episodes_graphs:
                episode_graph.write(os.path.join(episodes_graphs_folder, str(episode_number)))
                episode_number += 1
                

#####################################

class Trajectory_Graph:
    def __init__(self, sarsa_list):
        self.nodes = []
        self.edges = []
        self._populate_graph(sarsa_list)

    def _populate_graph(self, sarsa_list):
        step = 0
        for sarsa in sarsa_list:
            node1 = sarsa.current_state
            node2 = sarsa.next_state
            action = sarsa.current_action
            reward = sarsa.reward

            edge = Trajectory_Graph_Edge(node1, node2, action, reward, step)

            self.add_node(node1)
            self.add_node(node2)
            self.add_edge(edge)
            step += 1

    
    def add_nodes(self, visited_states_nodes):
        for node in visited_states_nodes:
            self.add_node(node)

    def add_node(self, visited_states_node):
        if visited_states_node not in self.nodes:
            self.nodes.append(visited_states_node)
        else:
            existing_node = self._find_node_in_graph(visited_states_node)
            return existing_node
    
    def _find_node_in_graph(self, visited_states_node):
        for search_node in self.nodes:
            if search_node == visited_states_node:
                return search_node
        return None
    
    def _find_edge_in_graph(self, visited_states_edge):
        for search_edge in self.edges:
            if search_edge == visited_states_edge:
                return search_edge
        return None

    def add_edges(self, visited_states_edges):
        for edge in visited_states_edges:
            self.add_edge(edge)

    def add_edge(self, visited_states_edge):
        if visited_states_edge in self.edges:
            existing_edge = self._find_edge_in_graph(visited_states_edge)
            self.edges.remove(existing_edge)
        self.edges.append(visited_states_edge)

    def write(self, graph_output_path):
        dot_graph = pgv.AGraph(strict = False, directed=True)
        for node in self.nodes:
            node_color = "black"
            if node.gave_reward is True:
                    node_color = "red"
            if node.hashcode == "":
                node.hashcode = "###"
            dot_graph.add_node(node.hashcode, label = str(node), color=node_color)

        for edge in self.edges:
            if edge.node1.hashcode =="":
                edge.node1.hashcode ="###"
            if edge.node2.hashcode =="":
                edge.node2.hashcode ="###"
            dot_graph.add_edge(edge.node1.hashcode, edge.node2.hashcode, weight= int(edge.reward), label= "{}: {},{}".format(edge.step_number, edge.action.action_name,edge.reward), **edge.kwargs)

        dot_graph.layout("dot")
        dot_graph.write(graph_output_path + ".dot")
        dot_graph.draw(graph_output_path + ".pdf")
    
    # def write(self, graph_output_path):
    #     dot_graph = pgv.AGraph(strict = False, directed=True)
    #     for node in self.nodes:
    #         dot_graph.add_node(int(node.id), label = str(node))

    #     for edge in self.edges:
    #         dot_graph.add_edge(edge.node1.id, edge.node2.id, weight= int(edge.reward), label= "{}: {},{}".format(edge.step_number, edge.action.action_name,edge.reward), **edge.kwargs)

    #     dot_graph.layout("dot")
    #     dot_graph.write(graph_output_path + ".dot")
    #     dot_graph.draw(graph_output_path + ".pdf")

#####################################

class Trajectory_Graph_Edge:
    def __init__(self, node1, node2, action, reward, step_number,  **kwargs):
        self.node1 = node1
        self.node2 = node2
        self.action = action
        self.reward = reward
        self.kwargs = kwargs
        self.step_number = step_number
    
    def update_attributes(self, **kwargs):
        self.kwargs = {**self.kwargs, **kwargs}   

    def __eq__(self, other):
        if isinstance(other, Trajectory_Graph_Edge):
            if self.node1 != other.node1:
                return False
            if self.node2 != other.node2:
                return False
            if self.action != other.action:
                return False
            return True
        return NotImplemented
        
    def __ne__(self, other):
        """Overrides the default implementation (unnecessary in Python 3)"""
        x = self.__eq__(other)
        if x is not NotImplemented:
            return not x
        return NotImplemented

    def __hash__(self):
        """Overrides the default implementation"""
        return hash(tuple([self.node1,self.node2, self.action]))