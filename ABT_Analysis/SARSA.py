from ABT_Action import ABT_Action
from ABT_State import ABT_State

class SARSA:
    def __init__(self, sarsa_JSON):
        self.current_state = ABT_State(sarsa_JSON["CurrentState"])
        self.current_action = ABT_Action(sarsa_JSON["CurrentAction"])
        self.reward = round(sarsa_JSON['Reward'], 4)
        self.next_state = ABT_State(sarsa_JSON["NextState"])
        self.next_action = ABT_Action(sarsa_JSON["NextAction"])