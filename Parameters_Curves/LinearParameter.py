from .ParameterCurve import ParameterCurve

class LinearParameter(ParameterCurve):
    def __init__(self, **kwargs):
        self.max_value = kwargs["max_value"]
        self.min_value = kwargs["min_value"]
        self.step = kwargs["step"]
        self._more_values = True
        if self.step > 0:
            self.current_value = self.min_value
        else:
            self.current_value = self.max_value

    def get_value(self):
        tmp = self.current_value
        _next = self.current_value + self.step
        if self._more_values and _next >= self.min_value and _next <= self.max_value:
            self.current_value = _next
        else:
            self._more_values = False
        return tmp
    
    def has_more_values(self):
        return self._more_values
    
if (__name__ == '__main__'):
    import sys
    if len(sys.argv) > 1:
        lp = LinearParameter(min_value=0, max_value=int(sys.argv[1]), step=1)
        while lp.has_more_values():
            print(lp.get_value())
        print("Done")
