from .ParameterCurve import ParameterCurve

class StaticParameter(ParameterCurve):
    
    def __init__(self, **kwargs):
        self.value = kwargs["value"]
    
    def get_value(self):
        return self.value

if (__name__ == '__main__'):
    import sys
    if len(sys.argv) > 1:
        sp = StaticParameter(value=int(sys.argv[1]))
        print(sp.get_value())
