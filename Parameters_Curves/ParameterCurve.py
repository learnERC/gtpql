from abc import ABC, abstractmethod

class ParameterCurve(ABC):
    @abstractmethod
    def get_value(self, **kwargs: None):
        pass