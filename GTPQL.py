#!/usr/bin/env python3

from Parameters_Curves import LinearParameter
from Parameters_Curves.StaticParameter import StaticParameter
from pydoc import locate
import sys
import yaml
import ansible_runner
import os
import shutil
import logging

###### ANSIBLE PLAYBOOKS ######
playbooks_folder = "GTPQL_Playbooks"
setup_playbooks_folder = playbooks_folder + os.path.sep + "Configuration"
explore_playbooks_folder = playbooks_folder + os.path.sep + "Agents_Run"
aggregate_playbooks_folder = playbooks_folder + os.path.sep + "Synchronization"
post_playbooks_folder = playbooks_folder + os.path.sep + "Post"
connectivity_playbook = playbooks_folder + os.path.sep + "Utilities" + os.path.sep + "Win_Connectivity_Test.yml"
###### ANSIBLE PLAYBOOKS ######

class GTPQL():
    def __init__(self, config_path, verbosity = 0):
        self.verbosity = verbosity

        self.config = yaml.safe_load(open(config_path))
    
        epsilon_class = locate(self.config["epsilon"]["type"])
        args = self.config["epsilon"]["args"]
        self.epsilon = epsilon_class(**args)

        alpha_class = locate(self.config["alpha"]["type"])
        args = self.config["alpha"]["args"]
        self.alpha = alpha_class(**args)

        gamma_class = locate(self.config["gamma"]["type"])
        args = self.config["gamma"]["args"]
        self.gamma = gamma_class(**args)

        self.starting_agent_path = self.config.get("starting_agent", "")
        self.experiment_folder_name = self.get_new_experiment_folder_name()

        self.epochs = self.config["epochs"]
        self.episodes_per_epoch = self.config["episodes_per_epoch"]
        self.steps_per_epoch = self.config["steps_per_epoch"]
        
        self.starting_epoch = self.config.get("starting_epoch", 0)
        self.run_single_host = self.config.get("run_single_host", False)

    def is_valid_ansible_run(stats):
        darks = GTPQL.__count_results(stats["dark"])
        failures = GTPQL.__count_results(stats["failures"])
        if darks > 0 or failures > 0:
            return False
        return True

    def run_ansible_playbook(self, playbook_path, extravars = None):
        r = ansible_runner.run(verbosity = self.verbosity, private_data_dir=".", playbook=playbook_path, extravars=extravars)
        return GTPQL.is_valid_ansible_run(r.stats)
    
    def run_ansible_playbooks(self, playbooks_paths, extravars = None):
        if os.path.isdir(playbooks_paths):
            for playbook_path in os.listdir(playbooks_paths):
                self.run_ansible_playbooks(playbooks_paths + os.path.sep + playbook_path, extravars)
        else:
            self.run_ansible_playbook(playbooks_paths, extravars)
        

    def check_connection(self):
        print()
        print("Connection check...")
        return self.run_ansible_playbook(connectivity_playbook)
    
    def __count_results(results):
        results_count = 0
        for key, value in results.items():
            results_count = results_count + value
        return results_count
    
    def get_new_experiment_folder_name(self):
        from datetime import datetime
        my_date = str(datetime.now())
        my_date = my_date.replace(" ", "_")
        my_date = my_date.replace(":", ".")
        return my_date

    def clean_local_cache():
        print("Cleaning local cache...")
        shutil.rmtree("tmp", ignore_errors=True)
        shutil.rmtree("env", ignore_errors=True)
        print("Done")
        print()

    def setup(self):
        print("*** Starting setup phase ***")
        GTPQL.clean_local_cache()
        is_valid = True
        is_valid = self.check_connection()
        extravars = None

        if self.starting_agent_path != "":
            extravars = {'timestamp': self.experiment_folder_name, 'source_starting_agent_path':self.starting_agent_path}
            if os.path.isfile(self.starting_agent_path):
                tmp = os.path.join(os.getcwd(), "tmp", self.experiment_folder_name)
                os.makedirs(tmp, exist_ok=True)
                shutil.copy(self.starting_agent_path, tmp)

        is_valid = self.run_ansible_playbooks(setup_playbooks_folder, extravars)
        return is_valid
    
    def run(self):
        gather_facts = 'no'
        first = True
        for cur_epoch in range(self.starting_epoch, self.epochs):
            if cur_epoch % 5 == 0 or first:
                gather_facts = 'yes'
                first = False
            else:
                gather_facts = 'no'
            self.run_epoch(cur_epoch, gather_facts)

    def run_epoch(self, cur_epoch, gather_facts):
        print()
        print("*** STARTING EPOCH " + str(cur_epoch) + " ***")
        print()

        extravars={'number_of_episodes': self.episodes_per_epoch,
        'number_of_steps': self.steps_per_epoch,
        'steps_per_epoch': self.steps_per_epoch,
        'timestamp': self.experiment_folder_name,
        'epoch':cur_epoch, 'epsilon': self.epsilon.get_value(),
        'alpha': self.alpha.get_value(),
        'gamma': self.gamma.get_value(),
        'gather': gather_facts,
        'run_single_host': self.run_single_host}

        #EXPLORE
        print("*** Exploration phase ***")
        self.run_ansible_playbooks(explore_playbooks_folder, extravars)

        #AGGREGATE
        if self.run_single_host:
            pass
        else:    
            print("*** Aggregation phase ***")
            self.run_ansible_playbooks(aggregate_playbooks_folder, extravars)
        print("*** EPOCH " + str(cur_epoch) + " FINISHED ***")
    
    def post(self):
        
        print("*** Starting post execution playbooks ***")
        extravars ={'timestamp': self.experiment_folder_name}
        self.run_ansible_playbooks(post_playbooks_folder, extravars)

#######################

def main(argv):
    print("*** Welcome to GTPQL ***")
    config_file = "GTPQL_config.yml"
    if len(sys.argv) > 1:
        config_file = sys.argv[1]
    gtpql = GTPQL(config_file)
    gtpql.setup()
    gtpql.run()
    gtpql.post()
    print("*** GTPQL stopped ***")


if __name__ == "__main__":
    main(sys.argv)