# GTPQL

## WORK IN PROGRESS
GTPQL is a work in progress and is currently subject to refactoring, please contact us at marco.mobilio@unimib.it to get the latest information regarding the latest branch and versions.

## Getting started

GTPQL is written in Python, all the requirements are listed in the `requirements.txt` file and can be installed using
```
pip3 install -r requirements.txt
```
To run the tool you just have to invoke

```
./GTPQL.py [config.yml]
```

## How to connect to the hosts
GTPQL exploits ansible for the connection, configuration, and executions of the Agents on the host machines. This means that the hosts must be configured in the `inventory\hosts` file (if not otherwised configured).

## The Ansible Playbooks
The Ansible playbooks used by GTPQL are in the `GTPQL_PLaybooks`folder, which has the following structure:
```
GTPQL_Playbooks
├── Agents_Run
│   ├── 1_ABT_Selenium_Run_Epoch.yml
│   └── 2_ABT_Selenium_End_Epoch.yml
├── Configuration
│   ├── ABT_Master_Setup.yml
│   ├── ABT_Selenium_Clean_Node.yml
│   └── ABT_Selenium_Setup.yml
├── Post
│   └── ABT_Selenium_End_Experiment.yml
├── Synchronization
│   └── ABT_Master_Run.yml
├── Utilities
│   ├── Win_Connectivity_Test.yml
│   ├── Win_Delete_ALL.yml
│   ├── Win_Reboot.yml
│   └── Win_Update.yml
└── vars
    └── external_vars.yml
```
You can optionally modify, add, or remove playbooks to adapt the tool to your use case, they will be executed once or more, depending on the folder and will be executed following the alphabetical order.
